(ns rangesearch.core)

(defrecord trange [from to origin])
(defrecord tnode [min-from max-from min-to max-to lleaf rleaf ranges])

(defn- create-node-rec [ranges]
  (when-first [r ranges]
    (if (== 1 (count ranges))
      (let [from (:from r)
            to (:to r)]
        (tnode. from from to to nil nil ranges))
      (let [from-s (map :from ranges)
            to-s (map :to ranges)
            pivot-idx (quot (count ranges) 2)]
        (tnode.
          (.from r)
          (reduce max from-s)
          (reduce min to-s)
          (reduce max to-s)
          (create-node-rec (subvec ranges 0 pivot-idx))
          (create-node-rec (subvec ranges pivot-idx))
          ranges)))))

(defn make-trange [[a b :as ab]]
  (->trange a b ab))

(defn create-node [ranges]
  (create-node-rec (vec (map make-trange (sort ranges)))))

(defn- search-rec [acc n a b]
  (cond
    (not n) acc
    (not
     (or
          (< (.min-to n) a)
          (> (.max-from n) b)))
    (reduce conj! acc (.ranges n))
    (<= (max
         (.min-from n) a)
        (min (.max-to n) b))
    (recur (search-rec acc (.lleaf n) a b) (.rleaf n) a b)
    :else acc))

(defn search-intersections [n [a b]]
  (map
    #(.origin ^trange %)
    (persistent! (search-rec (transient []) n a b))))

(defn make-searcher [ranges]
  (let [n (create-node ranges)]
    (fn [ab] (search-intersections n ab))))
