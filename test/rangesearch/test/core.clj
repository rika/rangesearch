(ns rangesearch.test.core
  (:use rangesearch.core)
  (:use clojure.test))

(defn search-intersections-lin [ranges [a b]]
 (filter (fn [[x y]] (and (>= (min b y) (max a x)))) ranges))

(defn rand-range [mx]
  #(let [a (rand-int mx)]
    [a (+ a (rand-int (- mx a)))]))

(defn minmax [ranges]
  (let [a (reduce min (map first ranges))
        b (reduce max (map second ranges))]
    [a b]))

(deftest test-small-compare-with-linear-search
  (let [ranges (take 5 (repeatedly (rand-range 30)))
        [ax bx] (minmax ranges)
        [a b] [(- ax 5) (+ bx 5)]
        search (make-searcher ranges)]
    (doseq [x (range a b) y (range x (inc b))]
      (is
        (= 
          (sort (search-intersections-lin ranges [x y]))
          (search [x y]))))))

(deftest test-big-compare-with-linear-search
  (let [ranges (take 10000 (repeatedly (rand-range 1000000)))
        search (make-searcher ranges)]
    (doseq [x (range 0 1000000 100000) y (range x 1000000 100000)]
      (is
        (= 
          (sort (search-intersections-lin ranges [x y]))
          (search [x y]))))))

